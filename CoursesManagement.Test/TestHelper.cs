﻿using System;
using System.Collections.Generic;
using CousesManagement.Interfaces.Service;
using CousesManagement.Models;
using CousesManagement.Services;
using Microsoft.EntityFrameworkCore;

namespace CoursesManagement.Tests
{
    public class TestHelper
    {
        private readonly CoursesManagementContext libraryDbContext;
        public TestHelper()
        {
            var builder = new DbContextOptionsBuilder<CoursesManagementContext>();
            builder.UseInMemoryDatabase(databaseName: "LibraryDbInMemory");
            var dbContextOptions = builder.Options;

            libraryDbContext = new CoursesManagementContext(dbContextOptions);
            libraryDbContext.Database.EnsureDeleted();
            libraryDbContext.Database.EnsureCreated();
            Seeder();
        }

        public ICourseService GetInMemoryCourseService()
        {
            return new CourseService(libraryDbContext);
        }

        public ICategoryService GetInMemoryCategoryService()
        {
            return new CategoryService(libraryDbContext);
        }

        public void Seeder()
        {
            libraryDbContext.Category.AddRange(
                    new Category
                    {
                        Name = "Comportamental"
                    },
                    new Category
                    {
                        Name = "Programação"
                    },
                    new Category
                    {
                        Name = "Qualidade"
                    },
                    new Category
                    {
                        Name = "Processos"
                    }
                );

            libraryDbContext.SaveChanges();
        }
    }
}
