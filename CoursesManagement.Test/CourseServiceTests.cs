﻿using System;
using System.Collections.Generic;
using CousesManagement.Models;
using CousesManagement.Models.Requests;
using Xunit;

namespace CoursesManagement.Tests
{
    public class CourseServiceTests
    {
        private readonly TestHelper _testHelper;
        
        public CourseServiceTests()
        {
            _testHelper = new TestHelper();
        }

        [Fact]
        public void Listar_Cursos()
        {
            var courseService = _testHelper.GetInMemoryCourseService();

            Assert.IsType<List<Course>>(courseService.List());
        }

        [Fact]
        public void Incluir_Novo_Curso()
        {
            string startName = "CURSO CRIADO TEST";
            // Repositories with InMemory Database
            var courseService = _testHelper.GetInMemoryCourseService();

            courseService.Store(new UpdateCourseRequest
            {
                Name = startName,
                StartDate = DateTime.Today.AddDays(10),
                EndDate = DateTime.Today.AddDays(30),
                StudentsLimit = 10,
                CategoryId = 1
            });


            var course = courseService.Find(1);

            // Valida se o registro foi feito corretamente
            Assert.NotNull(course);
            Assert.Equal(startName, course.Name);


            Exception ex = Assert.Throws<Exception>(() => courseService.Store(new UpdateCourseRequest
            {
                Name = "Curso 2",
                StartDate = DateTime.Today.AddDays(30),
                EndDate = DateTime.Today.AddDays(60),
                CategoryId = 1
            }));

            // Valida regra de periodo sobrescrever outro curso
            Assert.Equal("Existe(m) curso(s) planejados(s) dentro do período informado.", ex.Message);


            Exception ex1 = Assert.Throws<Exception>(() => courseService.Store(new UpdateCourseRequest
            {
                Name = "Curso 3",
                StartDate = DateTime.Today.AddDays(-200),
                EndDate = DateTime.Today.AddDays(-190),
                CategoryId = 1
            }));

            Assert.Equal("Não é permitido a inclusão de cursos com a data de início menor que a data atual.", ex1.Message);

            Exception ex2 = Assert.Throws<Exception>(() => courseService.Store(new UpdateCourseRequest
            {
                Name = "Curso 4",
                StartDate = DateTime.Today.AddDays(200),
                EndDate = DateTime.Today.AddDays(190),
                CategoryId = 1
            }));

            Assert.Equal("A data do fim do curso precisa ser superior a data de início.", ex2.Message);

        }


        [Fact]
        public void Atualizar_Curso()
        {
            string courseName = "CURSO ATUALIZADO TEST";
            // Repositories with InMemory Database
            var courseService = _testHelper.GetInMemoryCourseService();

            courseService.Store(new UpdateCourseRequest
            {
                Name = "CURSO 1",
                StartDate = DateTime.Today.AddDays(10),
                EndDate = DateTime.Today.AddDays(30),
                StudentsLimit = 10,
                CategoryId = 1
            });


            courseService.Update(1, new UpdateCourseRequest
            {
                Name = courseName,
                StartDate = DateTime.Today.AddDays(10),
                EndDate = DateTime.Today.AddDays(30),
                StudentsLimit = 5,
                CategoryId = 2
            });

            var course = courseService.Find(1);

            // Valida se o registro foi atualizado corretamente
            Assert.NotNull(course);
            Assert.Equal(courseName, course.Name);


            courseService.Store(new UpdateCourseRequest
            {
                Name = "CURSO 2",
                StartDate = DateTime.Today.AddDays(31),
                EndDate = DateTime.Today.AddDays(60),
                StudentsLimit = 10,
                CategoryId = 1
            });


            Exception ex = Assert.Throws<Exception>(() => courseService.Update(1, new UpdateCourseRequest
            {
                Name = "Curso 1",
                StartDate = DateTime.Today.AddDays(10),
                EndDate = DateTime.Today.AddDays(32),
                StudentsLimit = 5,
                CategoryId = 2
            }));

            // Valida regra de periodo sobrescrever outro curso
            Assert.Equal("Existe(m) curso(s) planejados(s) dentro do período informado.", ex.Message);


            Exception ex1 = Assert.Throws<Exception>(() => courseService.Update(1, new UpdateCourseRequest
            {
                Name = "Curso 1",
                StartDate = DateTime.Today.AddDays(-200),
                EndDate = DateTime.Today.AddDays(-190),
                StudentsLimit = 5,
                CategoryId = 2
            }));

            Assert.Equal("Não é permitido a inclusão de cursos com a data de início menor que a data atual.", ex1.Message);

            Exception ex2 = Assert.Throws<Exception>(() => courseService.Update(1, new UpdateCourseRequest
            {
                Name = "Curso 1",
                StartDate = DateTime.Today.AddDays(200),
                EndDate = DateTime.Today.AddDays(190),
                StudentsLimit = 5,
                CategoryId = 2
            }));

            Assert.Equal("A data do fim do curso precisa ser superior a data de início.", ex2.Message);

        }

    }
}
