﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CousesManagement.Models
{
    public class Course
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
        public int? StudentsLimit { get; set; }


        public virtual Category Category { get; set; }

        [Required]
        public int CategoryId { get; set; }
    }
}
