﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CousesManagement.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new CoursesManagementContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<CoursesManagementContext>>()))
            {
                // Look for any movies.
                if (context.Category.Any())
                {
                    return;   // DB has been seeded
                }

                context.Category.AddRange(
                    new Category
                    {
                        Name = "Comportamental"
                    },
                    new Category
                    {
                        Name = "Programação"
                    },
                    new Category
                    {
                        Name = "Qualidade"
                    },
                    new Category
                    {
                        Name = "Processos"
                    }
                );

                context.SaveChanges();

                var firstCategory = context.Category.FirstOrDefault();

                context.Course.AddRange(
                    new Course
                    {
                        Name = "Comportamental",
                        StudentsLimit = 10,
                        Category = firstCategory,
                        StartDate = DateTime.Now,
                        EndDate = DateTime.Now
                    }
                );

                context.SaveChanges();
            }
        }
    }
}
