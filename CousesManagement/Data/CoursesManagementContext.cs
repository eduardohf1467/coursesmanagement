using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CousesManagement.Models;

    public class CoursesManagementContext : DbContext
    {
        public CoursesManagementContext (DbContextOptions<CoursesManagementContext> options)
            : base(options)
        {
        }

    public DbSet<CousesManagement.Models.Category> Category { get; set; }

    public DbSet<CousesManagement.Models.Course> Course { get; set; }

}
