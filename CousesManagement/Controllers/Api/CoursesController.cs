﻿using System.Collections.Generic;
using CousesManagement.Interfaces.Service;
using CousesManagement.Models;
using CousesManagement.Models.Requests;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CousesManagement.Controllers.Api
{
    [Route("api/[controller]")]
    public class CoursesController : Controller
    {

        private readonly ICourseService _courseService;

        public CoursesController(ICourseService courseService)
        {
            _courseService = courseService;
        }

        // GET: api/values
        [HttpGet]
        public ActionResult<IEnumerable<Course>> Get()
        {
            try
            {
                return _courseService.List();
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var course = _courseService.Find(id);
                if (course == null)
                {
                    return NotFound("Curso não encontrado!");
                }

                return Ok(course);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] UpdateCourseRequest request)
        {
            try
            {
                _courseService.Store(request);
                return Ok();
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] UpdateCourseRequest request)
        {
            try
            {
                _courseService.Update(id, request);
                return Ok();
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _courseService.Destroy(id);
                return Ok();
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
