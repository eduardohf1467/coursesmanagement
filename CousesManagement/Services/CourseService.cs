﻿using System;
using System.Collections.Generic;
using System.Linq;
using CousesManagement.Models;
using CousesManagement.Models.Requests;
using CousesManagement.Interfaces.Service;

namespace CousesManagement.Services
{
    public class CourseService : ICourseService
    {
        private readonly CoursesManagementContext _context;

        public CourseService(CoursesManagementContext context)
        {
            _context = context;
        }

        public List<Course> List()
        {
            return _context.Course.ToList();
        }

        public Course Find(int id)
        {
            return _context.Course.Find(id);
        }

        public void Store(UpdateCourseRequest request)
        {

            ValidateRules(request);

            if (_context.Course.Any(x => x.StartDate <= request.EndDate && x.EndDate >= request.StartDate))
            {
                throw new Exception("Existe(m) curso(s) planejados(s) dentro do período informado.");
            }

            Category category = _context.Category.Find(request.CategoryId);

            if (category == null)
            {
                throw new Exception("Categoria não encontrada.");
            }

            Course course = new();

            course.Category = category;
            course.EndDate = request.EndDate;
            course.StartDate = request.StartDate;
            course.StudentsLimit = request.StudentsLimit;
            course.Name = request.Name;

            _context.Course.Add(course);
            _context.SaveChanges();

        }

        public void Update(int id, UpdateCourseRequest request)
        {
            Course course = Find(id);

            if(course == null)
            {
                throw new Exception("Curso não encontrado");
            }

            ValidateRules(request);

            if (_context.Course.Any(
                x => (x.StartDate <= request.EndDate && x.EndDate >= request.StartDate) && x.Id != id))
            {
                throw new Exception("Existe(m) curso(s) planejados(s) dentro do período informado.");
            }

            Category category = _context.Category.Find(request.CategoryId);

            if (category == null)
            {
                throw new Exception("Categoria não encontrada.");
            }

            course.Category = category;
            course.EndDate = request.EndDate;
            course.StartDate = request.StartDate;
            course.StudentsLimit = request.StudentsLimit;
            course.Name = request.Name;

            _context.SaveChanges();
        }

        private void ValidateRules(UpdateCourseRequest request)
        {
            if (request.StartDate < DateTime.Today)
            {
                throw new Exception("Não é permitido a inclusão de cursos com a data de início menor que a data atual.");
            }

            if (request.EndDate < request.StartDate)
            {
                throw new Exception("A data do fim do curso precisa ser superior a data de início.");
            }
        }

        public void Destroy(int id)
        {
            Course course = Find(id);

            if (course == null)
            {
                throw new Exception("Curso não encontrado");
            }

            _context.Course.Remove(course);
            _context.SaveChanges();
        }
    }

    
}
