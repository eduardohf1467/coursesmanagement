﻿using System;
using System.Collections.Generic;
using System.Linq;
using CousesManagement.Interfaces.Service;
using CousesManagement.Models;

namespace CousesManagement.Services
{
    public class CategoryService : ICategoryService
    {

        private readonly CoursesManagementContext _context;

        public CategoryService(CoursesManagementContext context)
        {
            _context = context;
        }

        public List<Category> List()
        {
            return _context.Category.ToList();
        }
    }
}
