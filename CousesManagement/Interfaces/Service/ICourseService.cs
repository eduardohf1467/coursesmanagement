﻿using System;
using System.Collections.Generic;
using CousesManagement.Models;
using CousesManagement.Models.Requests;

namespace CousesManagement.Interfaces.Service
{
    public interface ICourseService
    {
        public List<Course> List();

        public Course Find(int id);

        public void Store(UpdateCourseRequest request);

        public void Update(int id, UpdateCourseRequest request);

        public void Destroy(int id);
    }
}
