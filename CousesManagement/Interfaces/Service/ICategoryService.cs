﻿using System;
using System.Collections.Generic;
using CousesManagement.Models;

namespace CousesManagement.Interfaces.Service
{
    public interface ICategoryService
    {
        public List<Category> List();
    }
}
