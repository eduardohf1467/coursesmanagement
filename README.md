﻿## Projeto para gerenciamento de cursos

Este projeto tem como objetivo aplicar conhecimentos em .NET CORE para criação de uma API com CRUD básico de cursos.

Para fins de armazenamento de dados foi utilizado o banco Sqlite.

Para visualizar a documentação criada utilizando o Swagger, acesse a rota `localhost:port/swagger`. 

## Setup

`dotnet restore` 

`dotnet build` 

`dotnet test`

# 
## Questionário

1) Assinale as alternativas corretas sobre a linguagem C#:

- a. É uma linguagem fracamente tipada. 
>`- b. É uma linguagem fortemente tipada`.  
- c. É interpretada durante a execução. 
- d. É uma linguagem compilada e gerenciada. 

#

2) Defina, com suas palavras, o que é Intermediate Language (IL)?
>`R: É uma linguagem binária que é gerada pelo processo de compilação. Utilizando essa linguagem você consegue ter uma compatibilidade entre bibliotecas .net, sendo assim possível vc escrever uma biblioteca em VB e consumir em uma aplicação em C#.`
#
3) Comente sobre o que é o .Net Framework e o .Net Core.

>`R: O .NET Framework é um framework para desenvolvimento no windows, ele é composto por dois componentes principais: O CLR (Common Language Runtime), o mecanismo de execução que manipula os aplicativos em execução, e a biblioteca de classes .NET Framework, que oferece uma biblioteca imensa de códigos testados e reutilizáveis. Já o .NET CORE, é Cross-platform, então vc consegue desenvolver em qualquer ambiente. Digamos que o .net core é a versão melhorada do .net framework e open-source.`
#
4) O que são classes e objetos? Relacione os dois.

>`R: Colocando em termos mais leigos, a classe é a forma do bolo, enquanto o objeto é o bolo. A partir da classe você gera N objetos.`
#
5) O que é uma classe abstrata? (assinale as afirmações 
verdadeiras)

- a. É o mesmo que uma interface.
- b. Uma classe que, assim como uma interface, possui apenas assinaturas dos
métodos.
- c. Uma classe que possui apenas atributos e propriedades, herdados pelas
subclasses.
>`- d. Uma classe que possui implementação e assinatura de métodos e propriedades,
mas não pode ser instanciada.`
- e. Uma classe que possui implementação e assinatura de métodos e propriedades.
Pode ser instanciada com uso de singleton.
- f. Uma classe como outra qualquer, mas não pode implementar interfaces.
#
6) Qual a finalidade da palavra reservada virtual? Faça uma breve relação deste recurso com conceitos de Orientação à Objetos.
>`R: É utilizado quando vc precisa sobrescrever um método. Sendo assim, você declara o método com a palavra-chave virtual, e na classe derivada, vc utiliza o override.`
#
7) Descreva, resumidamente, o que significa cada letra da sigla SOLID.

>`R: [S]ingle Responsibility Principle (Princípio da Responsabilidade Única)
[O]pen/Closed Principle (Princípio do Aberto/Fechado)
[L]iskov Substitution Principle (Princípio da Substituição de Liskov)
[I]nterface Segregation Principle (Princípio da Segregação de Interfaces)
[D]ependency Inversion Principle (Princípio da Injeção de Dependência).`
#
8) Descreva, resumidamente, o que é Injeção de Dependência.

>`R: É um padrão de projeto usado para evitar o alto nível de acoplamento de código dentro de uma aplicação, com isso vc tem um aumento na facilidade de manutenção/implementação de novas funcionalidades e também habilita a utilização de mocks para realizar unit testes.`
#
9) Comente sobre como uma API Rest deve ser padronizada.

>`R: Os princípios do REST envolvem resources e cada um deve possuir uma URI única. Esses resources são manipulados usando solicitações HTTP. E cada método (GET, POST, PUT, PATCH, DELETE) tem um significado específico. Por exemplo, para apagar um registro, vc deve utilizar o método DELETE.`
#
10) Considerando ORM, micro-ORM e ADO.Net puro, indique quando e porque utilizar cada um.

>`R: ADO.NET é mais utilizado quando vc deseja ter uma camada que permite conectar-se ao db e modifica-lo usando conexões, comandos e parâmetros SQL, geralmente tem mais performance. 
O ORM permite mapear sua estrutura de banco de dados de maneira OOP: você pode adicionar, ler, atualizar, excluir registros no seu banco de dados usando objetos em C #, o que facilita muito o trabalho, evitando que vc precise escrever código SQL puro.
E os micro ORMs, são as versões Lite de ORM, permitem gravar consultas SQL e mapear parâmetros para propriedades de objetos, normalmente utilizado quando não deseja utilizar um ORM completo. Sendo assim, o micro ORM, por ser Lite, tende a ser mais rápido que o ORM completo.`
#
11) Quanto tempo você levaria para fazer uma funcionalidade com as operações CRUD (criar, recuperar/consultar, editar e excluir)?

>`R: Depende. O CRUD seria num projeto já existente? Com alguma regra de negócio já existente cujo o CRUD teria que encaixar? Seria necessário saber o contexto e efetuar uma analise do projeto antes de dar essa informação. Se for algo bem básico, e num projeto zerado, creio que em torno de 1 a 2 horas, sem contar o tempo de configuração de projeto e ambiente.`
#
12) O que é uma sealed class?

>`R: É uma classe selada, ou seja, ela já esta pronta para uso e não pode ser herdada.`
#




## License

[MIT license](https://opensource.org/licenses/MIT).
